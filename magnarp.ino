#include <Adafruit_NeoPixel.h>

#define NUMBER_OF_PIXELS = 60
#define DATA_PIN 6

//Adafruit_NeoPixel(number of pixels in strip, pin number (most are valid), pixel type flags, add together as needed)
Adafruit_NeoPixel strip = Adafruit_NeoPixel(60, DATA_PIN, NEO_GRB + NEO_KHZ800);

uint8_t r,g,b;
volatile boolean button1flag = LOW;
//volatile boolean button2flag = LOW;
volatile unsigned long interruptTime = 0;

void setup() {
  Serial.begin(9600);
  strip.begin();
  strip.show(); // Initialize all pixels to 'off'
  pinMode(2, INPUT_PULLUP);
  attachInterrupt(0, interrupt1, FALLING);
  delay(500); //delay half a second before sending data
}

void interrupt1()
{
  if((millis()-interruptTime) > 400) //'debounce' input
    button1flag = HIGH;
}

 /////////////////////////////////////////////////////////////////////////////////////////
 // HOW TO USE;
 // The functions are called below.
 // You can comment a function out by putting "//" before it.  Anything after "//" won't be compiled or put on the chip
 // You can re-order the functions by copying/pasting them.
 // general color codes can be found here - http://www.rapidtables.com/web/color/RGB_Color.htm
 // You can google a specific team's color codes - i.e. here are the vikings colors - http://teamcolorcodes.com/minnesota-vikings-color-codes/
 // Dark colors are tough with this lamp. You may need to decrease all the red, green, and blue values a bit so it looks better.
 // You can call a function as many times as you want with different colors, as illustrated below with 
 //    calling twoColorsScrolling() twice, once with queens and once with cornhuskers colors
 //Vikings - Purple-59,1,96  Gold-240,191,0
 //Huskers - Scarlet-208,0,0  Cream- 254,253,250
 //America - Old Glory Red-224,22,43 white-255,255,255 Old Glory Blue-0,82,165
 ///////////////////////////////////////////////////////////////////////////////////////// main
 
void loop()
{
//  solid_color(200,200,200);
//  solid_color(255,0,0);
//  solid_color(0,255,0);
//  solid_color(0,0,255);
  rainbowCycle(50);
  rainbow(50);
  smooth_shift(50,3000); //(transitionDelay, time between transitions)
  random_waterfall(10);//(delay) changes to a random color when it hits the end
  color_waterfall(5,59,1,96,240,191,0); //(delay, r1, g1, b1, r2, g2, b2) //QUEENS
  color_waterfall(5,4,26,200,255,2,5); //(delay, r1, g1, b1, r2, g2, b2) //TWINS
//  color_waterfall(0,208,0,0,254,253,250); //(delay, r1, g1, b1, r2, g2, b2) //TWINS
//  twoColorsScrolling(59,1,96,240,191,0,75); //scrolling two colors (red1,green1,blue1,red2,green2,blue2,delay) //QUEENS //where a color is a value 0-255 and delay will set speed (in milliseconds, more delay=slower)
  twoColorsScrolling(208,0,0,254,253,250,75); //scrolling two colors (red1,green1,blue1,red2,green2,blue2,delay) //HUSKERS
  twoColorsScrolling(4,26,200,255,2,5,75); //scrolling two colors (red1,green1,blue1,red2,green2,blue2,delay) //TWINS 
 
}


//////////////////////////////////////////////////////////////////////////////////////////////////// SOLID COLOR
void solid_color(uint8_t r,uint8_t g,uint8_t b)
{
  button1flag = LOW;   interruptTime = millis();
  
  while(1)
  {
    for(uint8_t i=0; i<=strip.numPixels(); i++)
    {
      strip.setPixelColor(i,r,g,b); //200,200,200 max
    }
    strip.show();
    if(button1flag == HIGH){return;}
  }
}

//////////////////////////////////////////////////////////////////////////////////////////////////// COLOR WATERFALL
void  color_waterfall(uint8_t time, uint8_t r1,uint8_t g1,uint8_t b1,uint8_t r2,uint8_t g2,uint8_t b2)
{
  Serial.println("color waterfall");
  float rs,gs,bs,rf,gf,bf,rt,gt,bt; //s = step, f=float,t=temporary
  
  button1flag = LOW;   interruptTime = millis();
  
  int stepSize = time*2; //set the steps between color transitions
  
  for(uint8_t i=0; i<=strip.numPixels(); i++)
  {
    strip.setPixelColor(i,r2,g2,b2);
  }
  strip.show();
  
  while(1)
  { 
    rt=r2;gt=g2;bt=b2; 
    r2=r1;g2=g1;b2=b1; //make rgb2 the first color: rgb1
    r1=rt;g1=gt;b1=bt;
    Serial.print("r1= ");Serial.print(r1); Serial.print("\t");Serial.print(g1); Serial.print("\t");Serial.print(b1); Serial.println();
    Serial.print("r2= ");Serial.print(r2); Serial.print("\t");Serial.print(g2); Serial.print("\t");Serial.print(b2); Serial.println();
    
    rs=(r1-r2)/stepSize;
    gs=(g1-g2)/stepSize;
    bs=(b1-b2)/stepSize;
    
    if(r1>r2 || rs<0) rs*=-1; //check the step and make sure it is oriented right
    if(g1>g2 || gs<0) gs*=-1;
    if(b1>b2 || bs<0) bs*=-1;
    
    Serial.print("rs= ");Serial.print(rs); Serial.print("\t");Serial.print(gs); Serial.print("\t");Serial.print(bs); Serial.println();
    
    for(int i=strip.numPixels(); i>=0; i--) //pick a pixel
    {
      for(uint8_t j = 0; j<=stepSize; j++) //shift the color
      {
        rf=(r1+(j*rs));
        gf=(g1+(j*gs));
        bf=(b1+(j*bs));
        //Serial.print(rf); Serial.print("\t"); Serial.print(gf); Serial.print("\t"); Serial.println(bf);
        strip.setPixelColor(i,rf,gf,bf);
        strip.show();
        delay(time);
      }
      if(button1flag == HIGH){return;}
      strip.setPixelColor(i,r2,g2,b2);
      strip.show();
    }
    
    ///////////////
    
    rt=r2;gt=g2;bt=b2; 
    r2=r1;g2=g1;b2=b1; //make rgb2 the first color: rgb1
    r1=rt;g1=gt;b1=bt;
       
    Serial.print("r1= ");Serial.print(r1); Serial.print("\t");Serial.print(g1); Serial.print("\t");Serial.print(b1); Serial.println();
    Serial.print("r2= ");Serial.print(r2); Serial.print("\t");Serial.print(g2); Serial.print("\t");Serial.print(b2); Serial.println();
    
    rs=(r1-r2)/stepSize;
    gs=(g1-g2)/stepSize;
    bs=(b1-b2)/stepSize;
    
    if(r1>r2 || rs<0) rs*=-1; //check the step and make sure it is oriented right
    if(g1>g2 || gs<0) gs*=-1;
    if(b1>b2 || bs<0) bs*=-1;
    
    Serial.print("rs= ");Serial.print(rs); Serial.print("\t");Serial.print(gs); Serial.print("\t");Serial.print(bs); Serial.println();
    
    for(int i=0; i<=strip.numPixels(); i++) //pick a pixel
    {
      for(uint8_t j = 0; j<=stepSize; j++) //shift the color
      {
        rf=(r1+(j*rs));
        gf=(g1+(j*gs));
        bf=(b1+(j*bs));
        //Serial.print(rf); Serial.print("\t"); Serial.print(gf); Serial.print("\t"); Serial.println(bf);
        strip.setPixelColor(i,rf,gf,bf);
        strip.show();
        delay(time);
      }
      if(button1flag == HIGH){return;}
      strip.setPixelColor(i,r2,g2,b2);
      strip.show();
    }
  }
}

//////////////////////////////////////////////////////////////////////////////////////////////////// RANDOM WATERFALL
void  random_waterfall(uint8_t time)
{
  Serial.println("random waterfall");
  float r1,g1,b1,r2,g2,b2; //make these ints?
  float rs,gs,bs,rf,gf,bf;
  
  int stepSize = time*2; //set the steps between color transitions
  
  button1flag = LOW;   interruptTime = millis();
  
  Wheel2(rand() % 255); //pick random r,g,b colors
  r2=r;g2=g;b2=b;
  for(uint8_t i=0; i<=strip.numPixels(); i++) //initialize to rgb2
  {
    strip.setPixelColor(i,r2,g2,b2);
  }
  strip.show();
  
  while(1)
  {
    r1=r2;g1=g2;b1=b2; //make rgb2 the first color: rgb1
    Wheel2(rand() % 255);
    r2=r;g2=g;b2=b; //pick random rgb2
    
    Serial.print("r1= ");Serial.print(r1); Serial.print("\t");Serial.print(g1); Serial.print("\t");Serial.print(b1); Serial.println();
    Serial.print("r2= ");Serial.print(r2); Serial.print("\t");Serial.print(g2); Serial.print("\t");Serial.print(b2); Serial.println();
    
    rs=(r1-r2)/stepSize;
    gs=(g1-g2)/stepSize;
    bs=(b1-b2)/stepSize;
    
    if(r1>r2 || rs<0) rs*=-1; //check the step and make sure it is oriented right
    if(g1>g2 || gs<0) gs*=-1;
    if(b1>b2 || bs<0) bs*=-1;
    
    Serial.print("rs= ");Serial.print(rs); Serial.print("\t");Serial.print(gs); Serial.print("\t");Serial.print(bs); Serial.println();
    
    for(int i=strip.numPixels(); i>=0; i--) //pick a pixel
    {
      for(uint8_t j = 0; j<=stepSize; j++) //shift the color
      {
        rf=(r1+(j*rs));
        gf=(g1+(j*gs));
        bf=(b1+(j*bs));
        //Serial.print(rf); Serial.print("\t"); Serial.print(gf); Serial.print("\t"); Serial.println(bf);
        strip.setPixelColor(i,rf,gf,bf);
        strip.show();
        delay(time);
      }
      if(button1flag == HIGH){return;}
      strip.setPixelColor(i,r2,g2,b2);
      strip.show();
    }
    
    ///////////////
    
    r1=r2;g1=g2;b1=b2;
    Wheel2(rand() % 255);
    r2=r;g2=g;b2=b;
    
    Serial.print("r1= ");Serial.print(r1); Serial.print("\t");Serial.print(g1); Serial.print("\t");Serial.print(b1); Serial.println();
    Serial.print("r2= ");Serial.print(r2); Serial.print("\t");Serial.print(g2); Serial.print("\t");Serial.print(b2); Serial.println();
    
    rs=(r1-r2)/stepSize;
    gs=(g1-g2)/stepSize;
    bs=(b1-b2)/stepSize;
    
    if(r1>r2 || rs<0) rs*=-1; //check the step and make sure it is oriented right
    if(g1>g2 || gs<0) gs*=-1;
    if(b1>b2 || bs<0) bs*=-1;
    
    Serial.print("rs= ");Serial.print(rs); Serial.print("\t");Serial.print(gs); Serial.print("\t");Serial.print(bs); Serial.println();
    
    for(int i=0; i<=strip.numPixels(); i++) //pick a pixel
    {
      for(uint8_t j = 0; j<=stepSize; j++) //shift the color
      {
        rf=(r1+(j*rs));
        gf=(g1+(j*gs));
        bf=(b1+(j*bs));
        //Serial.print(rf); Serial.print("\t"); Serial.print(gf); Serial.print("\t"); Serial.println(bf);
        strip.setPixelColor(i,rf,gf,bf);
        strip.show();
        delay(time);
      }
      if(button1flag == HIGH){return;}
      strip.setPixelColor(i,r2,g2,b2);
      strip.show();
    }
  }
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////smooth_shift 
void smooth_shift(int transitionTime, int delayTime)
{
  float r1,g1,b1,r2,g2,b2; //make these ints?
  float rs,gs,bs,rf,gf,bf;
  
  int stepSize = transitionTime; //find the step
  
  button1flag = LOW;   interruptTime = millis();
  
  for(uint8_t i=strip.numPixels();i>0;i--) //set initial color
  {
    strip.setPixelColor(i,r2,g2,b2);
  }
  strip.show();
  
  while(1)
  {
    r1=r2;g1=g2;b1=b2;
    Serial.print(r1); Serial.print("\t");Serial.print(g1); Serial.print("\t");Serial.print(b1); Serial.println();
    Wheel2(rand() % 255);
    r2=r;g2=g;b2=b;
    Serial.print(r2); Serial.print("\t");Serial.print(g2); Serial.print("\t");Serial.print(b2); Serial.print("\t");
    
    rs=(r1-r2)/stepSize;
    gs=(g1-g2)/stepSize;
    bs=(b1-b2)/stepSize;
    
    if(r1>r2 || rs<0) rs*=-1; //check the step and make sure it is oriented right
    if(g1>g2 || gs<0) gs*=-1;
    if(b1>b2 || bs<0) bs*=-1;
    Serial.print(rs); Serial.print("\t");Serial.print(gs); Serial.print("\t");Serial.print(bs); Serial.println();
    
    for(uint8_t j = 0; j<=stepSize; j++) //shift the color
    {
      r=int(r1+(j*rs));
      g=int(g1+(j*gs));
      b=int(b1+(j*bs));
      //Serial.print(r); Serial.print("\t"); Serial.print(g); Serial.print("\t"); Serial.println(b);
      for(int i=strip.numPixels(); i>=0; i--)
      {
        strip.setPixelColor(i,r,g,b);
      }
      if(button1flag == HIGH){return;}
      strip.show();
      delay(transitionTime);
    }
    for(int i = 0; i < delayTime; i++)
    {
      if(button1flag == HIGH){return;}
      delay(1);
    }
  }
}

//  /////////////////////////////////////////////////////////////////////////// twoColorsScrolling
void twoColorsScrolling(uint32_t red1,uint32_t blue1,uint32_t green1,uint32_t red2,uint32_t blue2,uint32_t green2, uint16_t factor){
  
  Serial.println ("twoColorsScrolling");
  uint8_t i=0,j=0,start=0; //declare vairables
  
  button1flag = LOW;   interruptTime = millis();
  
  while(1) //infinite loop (until button is pressed)
  {    
  while(start<30)
  { 
    for(i=0; i<60; i++)
    {
      if(i>=start && i<start+30)
        strip.setPixelColor(i,red1,blue1,green1); //set pixel 'i' to color'xxx,xxx,xxx' where xxx is 0-255
      else
        strip.setPixelColor(i,red2,blue2,green2); //set pixel 'i' to color'xxx,xxx,xxx' where xxx is 0-255
        //strip.show();//send color
    }
    if(button1flag == HIGH){return;}
    strip.show();//send color
    start++; //increment start
    delay(factor);
    }
    
    start=0;
    
    while(start<30)
    { 
    for(i=0; i<60; i++)
    {
    if(i>=start && i<start+30)
    strip.setPixelColor(i,red2,blue2,green2); //set pixel 'i' to color'xxx,xxx,xxx' where xxx is 0-255
    else
    strip.setPixelColor(i,red1,blue1,green1); //set pixel 'i' to color'xxx,xxx,xxx' where xxx is 0-255
    }
    if(button1flag == HIGH){return;}
    strip.show();//send color
    start++; //increment start
    delay(factor);
    }
    start=0;

  }
  }

//////////////////////////////////////////////////////////////////////////////////////////// RAINBOW CYCLE
// Slightly different, this makes the rainbow equally distributed throughout
void rainbowCycle(uint8_t time)
{
  Serial.println ("Rainbow Cycle");
  button1flag = LOW;   interruptTime = millis();
  
  while(1)
  {
    for(uint16_t j=0; j<256*5; j++) // 5 cycles of all colors on wheel
    { 
      for(uint16_t i=0; i< strip.numPixels(); i++) 
      {  
        strip.setPixelColor(i, Wheel(((i * 256 / strip.numPixels()) + j) & 255)); 
      }
      if(button1flag == HIGH){return;}
      strip.show();
      delay(time);
    }
  }
}

////////////////////////////////////////////////////////////////////////////////////////////////// RAINBOW
void rainbow(uint8_t time) 
{
  Serial.println ("Rainbow");
  button1flag = LOW;   interruptTime = millis();
  
  while(1)
  {
    for(uint16_t j=0; j<256; j++) 
    {
      for(uint16_t i=0; i<strip.numPixels(); i++) 
      {
        strip.setPixelColor(i, Wheel((i+j) & 255));
      }
      if(button1flag == HIGH){return;}
      strip.show();
      delay(time);
    }
  }
}

//////////////////////////////////////////////////////////////////////// COLOR WHEEL
// Input a value 0 to 255 to get a color value.
// The colours are a transition r - g - b - back to r.
uint32_t Wheel(byte WheelPos) {
  if(WheelPos < 85) {
   return strip.Color(WheelPos * 3, 255 - WheelPos * 3, 0);
  } else if(WheelPos < 170) {
   WheelPos -= 85;
   return strip.Color(255 - WheelPos * 3, 0, WheelPos * 3);
  } else {
   WheelPos -= 170;
   return strip.Color(0, WheelPos * 3, 255 - WheelPos * 3);
  }
} 

//////////////////////////////////////////////////////////////////////// COLOR WHEEL 2
// Input a value 0 to 255 to get a color value.
// The colors are a transition r - g - b - back to r.
void Wheel2(byte WheelPos) {
  if(WheelPos < 85) {
    r= WheelPos * 3;
    g= 255 - WheelPos * 3;
    b= 0;
  }
  else if(WheelPos < 170) {
    WheelPos -= 85;
    r= 255 - WheelPos * 3;
    g= 0;
    b= WheelPos * 3;
  }
  else {
    WheelPos -= 170;
    r= 0;
    g= WheelPos * 3;
    b= 255 - WheelPos * 3;
  }
}

////////////////////////////////////////////////////////////////////// SET DELAY
//int set_delay(uint8_t factor, uint8_t c)
//{
//  ///button2flag sets the delay
//  while(digitalRead(buttonPin2) ==0)
//  {
//    Serial.println(factor);
//  factor += 1;
//  delay(18);
//  for(uint16_t i=0;i<factor/4.25;i++){
//  strip.setPixelColor(i,255,255,255);
//  }
//  strip.show();
//  
//  if(factor==255)
//  {
//  factor = 0;
//  }
//  /*
//  for(uint16_t i=strip.numPixels();i>0;i--)
//  {
//  strip.setPixelColor(i,Wheel(c));
//  }
//  strip.show();
//  */
//  return factor;
//  }
//}
